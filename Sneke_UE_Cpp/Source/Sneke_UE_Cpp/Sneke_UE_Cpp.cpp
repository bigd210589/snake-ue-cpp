// Copyright Epic Games, Inc. All Rights Reserved.

#include "Sneke_UE_Cpp.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Sneke_UE_Cpp, "Sneke_UE_Cpp" );
