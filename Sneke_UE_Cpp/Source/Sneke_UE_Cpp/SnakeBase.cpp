// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnekeElimentBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElimentSize = 100.f;
	LasMoveDirectiom = EMovementDirection::DOWN;
	MovementSpeed = 10.f;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeEliments(4);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeEliments(int ElementNum)
{
	for (int i = 0; i < ElementNum; ++i)
	{
		FVector NewLocation(SnekeEliments.Num() * ElimentSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnekeElimentBase* NewSnakeElem = GetWorld()->SpawnActor<ASnekeElimentBase>(SnekeElimentClasss, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemInd = SnekeEliments.Add(NewSnakeElem);
		if (ElemInd == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector; 
	float MovementSpid = ElimentSize;
	
	switch (LasMoveDirectiom)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpid;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementSpid;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MovementSpid;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= MovementSpid;
		break;
	}

	SnekeEliments[0]->ToggelCollision();

	for (int i = SnekeEliments.Num() - 1; i > 0; --i)
	{
		auto CurentElement = SnekeEliments[i];
		auto PrevElement = SnekeEliments[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation(); 
		CurentElement->SetActorLocation(PrevLocation);
	}

	SnekeEliments[0]->AddActorWorldOffset(MovementVector);
	SnekeEliments[0]->ToggelCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnekeElimentBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnekeEliments.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteracteble* InteractebleInterfase = Cast<IInteracteble>(Other);
		if (InteractebleInterfase)
		{
			InteractebleInterfase->Interact(this, bIsFirst);
		}
	}
}