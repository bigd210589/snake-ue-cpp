// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayrPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"

// Sets default values
APlayrPawnBase::APlayrPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
}

// Called when the game starts or when spawned
void APlayrPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor(); 
}

// Called every frame
void APlayrPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayrPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &APlayrPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayrPawnBase::HandlePlayerHorizontalInput);
}

void APlayrPawnBase::CreateSnakeActor()
{
	SnakeAktor = GetWorld()->SpawnActor<ASnakeBase>(SnakeAktorClass, FTransform());
}

void APlayrPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeAktor))
	{
		if(value > 0 && SnakeAktor->LasMoveDirectiom != EMovementDirection::DOWN)
		{
			SnakeAktor->LasMoveDirectiom = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeAktor->LasMoveDirectiom != EMovementDirection::UP)
		{
			SnakeAktor->LasMoveDirectiom = EMovementDirection::DOWN;
		}
	}
}

void APlayrPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeAktor))
	{
		if (value > 0 && SnakeAktor->LasMoveDirectiom != EMovementDirection::LEFT)
		{
			SnakeAktor->LasMoveDirectiom = EMovementDirection::RIGHT;
		}
		else if (value < 0 && SnakeAktor->LasMoveDirectiom != EMovementDirection::RIGHT)
		{
			SnakeAktor->LasMoveDirectiom = EMovementDirection::LEFT;
		}
	}
}