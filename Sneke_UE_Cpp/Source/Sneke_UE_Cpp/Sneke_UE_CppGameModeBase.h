// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Sneke_UE_CppGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNEKE_UE_CPP_API ASneke_UE_CppGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
